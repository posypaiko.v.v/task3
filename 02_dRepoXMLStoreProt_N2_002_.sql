IF	OBJECT_ID('dbo.RepoXMLStoreProt_N2_TAB')	IS NOT NULL
	DROP	TABLE dbo.RepoXMLStoreProt_N2_TAB
GO

CREATE	TABLE	dbo.RepoXMLStoreProt_N2_TAB
(	TableId				TRowId		NOT NULL
,	[No]				TFlag		NOT NULL
---------------------------------------------------------
,	TreatyId			TRowId		NOT NULL	-- Id ��������
,	OrgDate				TCode		NULL
,	DateFrom			TCode		NULL
,	DateInto			TCode		NULL
,	R020				TTag		NULL
,	AmountId			TRowId		NOT NULL	-- Id �����
,	ClientId			TRowId		NOT NULL	-- Id �������
,	Confirmed			TMoney		NULL		DEFAULT($0)	--������� �� �����
,	ConfirmedPrevKv			TMoney		NULL		DEFAULT($0)	--������� �� ����� �� ���������� �������� ����
,	ConfirmedPrevM			TMoney		NULL		DEFAULT($0)	--������� �� ����� �� ���������� �������� ����
,	DTurns				TMoney		NULL		DEFAULT($0)	--������ �� �-��-����������
,	DTurns2				TMoney		NULL		DEFAULT($0)	--������ �� �-��-����������
,	DTurns3				TMoney		NULL		DEFAULT($0)	--������ �� �-��-����������
,	CTurns				TMoney		NULL		DEFAULT($0)	--������ �� �-��-����������
,	DTurnsCrncy			TMoney		NULL		DEFAULT($0)	--������ �� �-��-�������
,	DTurnsCrncy2			TMoney		NULL		DEFAULT($0)	--������ �� �-��-�������
,	DTurnsCrncy3			TMoney		NULL		DEFAULT($0)	--������ �� �-��-�������
,	CTurnsCrncy			TMoney		NULL		DEFAULT($0)	--������ �� �-��-�������
,	T100_EKP010203			TMoney		NULL		DEFAULT($0)	--������ �� EKP_AN2001,EKP_AN2002,EKP_AN2003
,	T100_EKP04			TMoney		NULL		DEFAULT($0)	--������ �� EKP_AN2004
,	EKP_01				TInt		NULL
,	EKP_02				TInt		NULL
,	EKP_03				TInt		NULL
,	EKP_04				TInt		NULL
,	EKP_05				TInt		NULL
,	EKP_06				TInt		NULL
,	EKP_07				TInt		NULL
,	EKP_08				TInt		NULL
,	EKP_09				TInt		NULL
,	EKP_10				TInt		NULL
,	EKP_11				TInt		NULL
,	InFileCode			TCode		NULL
,	DateDefolt			TDate		NULL
,	R030				TTag		NULL				--�� ��������� F8
,	S260				TTag		NULL				--�� ��������� F8
,	S031				TTag		NULL				--�� ��������� F8
,	S032				TTag		NULL				--�� ��������� F8
,	KU				TTag		NULL				--�� ��������� D51
,	K070				TCode		NULL		DEFAULT('14430')--�� ���������� �������
,	K072				TTag		NULL				--�� ��������� D51		
,	F048				TTag		NULL				--�� ��������� D51
,	F034				TTag		NULL				--�� ��������� F8
,	S130				TTag		NULL		DEFAULT('#')		--������ � CCR
,	DSTI				TTag		NULL		DEFAULT('9')		--�������� ���� � �������� �������
,	DSTI_NUM			TMoney		NULL		DEFAULT(0)	--���� Confirmed �� ��������� �7 ����� � S240 = 1,2,I
,	DSTI_DENOM			TMoney		NULL		DEFAULT(0)	--���� ISNULL(c.ConfirmedIncome,$0)+ISNULL(c.UnconfirmedIncome,$0)  � ������� dbo.ClientCreditRegistryExtensions
,	N140				TTag		NULL		DEFAULT('7')	,
 
	CONSTRAINT IRepoXMLStoreProt_N2_TABIdentity	UNIQUE	CLUSTERED
	(	TableId
	,	[No]
	)	,
)
GO

GRANT	SELECT, UPDATE, INSERT
ON	dbo.RepoXMLStoreProt_N2_TAB
TO	sc_Servers
GO
