MERGE	dbo.RepoXMLTimes AS x  
USING	(	
---------------------------------------------------------------------------------------------------
--	0x0001	- �� ����������
--	0x0002	- ������� (������� ������� ����)
--	0x0004	- ������� (������� ������� ����)
--	0x0008	- ���������� (1,6,11,16,21,26 ����� �i���)
--	0x0010	- ������������
--	0x0020	- ��������
--	0x0040	- ����������� (1,16 ����� �i���)
--	0x0080	- �������
--	0x0100	- �����������
--	0x0200	- �����������
--	0x0400	- �������
VALUES	('N1',	'���� N1 (����.�� ����� N1.)',	0x0080,	13, 0)
---------------------------------------------------------------------------------------------------
	)	AS y 
(A010, Info, Period, Days, Months)
	ON	y.A010		= x.A010
WHEN	MATCHED	THEN
	UPDATE	
	SET	Info		     = y.Info
	,	Period		= y.Period
	,	Days		     = y.Days
	,	Months		= y.Months
	,	Status		= 1
WHEN	NOT MATCHED BY TARGET THEN
	INSERT	(A010, Info, Period, Days, Months, Status) 
	VALUES	(y.A010, y.Info, y.Period, y.Days, y.Months, 1);
GO