DECLARE	@HistoryTables TABLE
(
	[OriginName]		SYSNAME
,	[TargetName]		SYSNAME
,	[Schema]		SYSNAME
);

INSERT INTO @HistoryTables
(	[OriginName]
,	[TargetName]
,	[Schema]
)
VALUES	-- DBO
	('RepoStore_ApplicationsN1'			, 'RepoStore_ApplicationsN1_TAB'		, 'dbo')

DECLARE	@RequestTemplate	VARCHAR(max) =
'
IF	object_id(''@schema.@TargetName'', ''U'') IS NULL
AND	object_id(''@schema.@OriginName'', ''U'') IS NOT NULL
	EXEC sp_rename ''@schema.@OriginName'', ''@TargetName''
';

DECLARE	@OriginName	SYSNAME
,	@TargetName	SYSNAME
,	@Schema		SYSNAME
,	@Request	VARCHAR(max);

DECLARE	_TablesScroll CURSOR LOCAL STATIC READ_ONLY
FOR	SELECT	[OriginName]
	,	[TargetName]
	,	[Schema]
	FROM	@HistoryTables
	ORDER
	BY	[Schema]
	,	[OriginName];

OPEN	_TablesScroll;

WHILE 1=1 BEGIN

	FETCH NEXT FROM
		_TablesScroll
	INTO	@OriginName
	,	@TargetName
	,	@Schema;

	IF	@@FETCH_STATUS != 0	BREAK;

	SET	@Request	= REPLACE( @RequestTemplate	, '@OriginName'	, @OriginName	);
	SET	@Request	= REPLACE( @Request		, '@TargetName'	, @TargetName	);
	SET	@Request	= REPLACE( @Request		, '@Schema'	, @Schema	);

	PRINT	FORMATMESSAGE('Processing %s.%s', @Schema, @OriginName);

	EXEC(@Request);
END;

CLOSE		_TablesScroll;
DEALLOCATE 	_TablesScroll;
GO
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------

IF	OBJECT_ID('dbo.RepoStore_ApplicationsN1_TAB')	IS NULL
BEGIN
	CREATE	TABLE	dbo.RepoStore_ApplicationsN1_TAB
	(	Id		TRowId		IDENTITY
	,	DateAppl	TDate		NOT NULL	-- ���� ���������� ������ (������������� ������������ ������)
	,	IdAppl		TRowId		NOT NULL	-- ID ������ WB
	,	CodeStatAppl	TCode		NULL		-- ������ ������ �� �� (���)
	,	NameStatAppl	TInFo		NULL		-- ������ ������ �� �� (������������)
	,	DateConcl	TDate		NULL		-- ���� �������� ������ � ������ �� �� (������������� ������������ ������)
	,	ProductId	TRowId		NULL		-- ID �������� ��� (�����������)
	,	ProductName	TInfo		NULL		-- ���� ID �������� ��� ���, �� ������������ �������� WB
	,	ClientId	TRowId		NOT NULL	-- �� ������� � ���
	,	Confirmed	TMoney		NOT NULL	-- ����� � ��������
	,	CurrencyTag	TTag		NOT NULL	-- ������
	,	BranchId	TRowId		NOT NULL	-- �� ������� � ���
	,	TreatyId	TRowId		NULL		-- ID �������� ���
	,	ReportDate	TDate		NOT NULL	-- ���� �������� ������ �� �������� � ��� (��������� ����������� ���� ������)												,
	,	SourceCode	TCode		NOT NULL	-- ��� ��������� (SourceCode = WebBank)

	,	CONSTRAINT IRepoStore_ApplicationsN1_TAB_Identity
		PRIMARY	KEY
		(	ReportDate
		,	Id
		)	,
	)
END
GO
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
DECLARE	@HistoryTables TABLE
(
	[OriginName]		SYSNAME
,	[TargetName]		SYSNAME
,	[Schema]		SYSNAME
);

INSERT INTO @HistoryTables
(	[OriginName]
,	[TargetName]
,	[Schema]
)
VALUES	-- DBO
	('RepoStore_ApplicationsN1'			, 'RepoStore_ApplicationsN1_TAB'		, 'dbo')

DECLARE	@RequestTemplate	VARCHAR(max) =
'
IF	NOT EXISTS(SELECT 1 FROM sys.synonyms as x WHERE x.schema_id = SCHEMA_ID(''@schema'') AND x.Name = ''@OriginName'')
AND	object_id(''@schema.@TargetName'', ''U'') IS NOT NULL
	CREATE SYNONYM @schema.@OriginName FOR @schema.@TargetName;
';

DECLARE	@OriginName	SYSNAME
,	@TargetName	SYSNAME
,	@Schema		SYSNAME
,	@Request	VARCHAR(max);

DECLARE	_TablesScroll CURSOR LOCAL STATIC READ_ONLY
FOR	SELECT	[OriginName]
	,	[TargetName]
	,	[Schema]
	FROM	@HistoryTables
	ORDER
	BY	[Schema]
	,	[OriginName];

OPEN	_TablesScroll;

WHILE 1=1 BEGIN

	FETCH NEXT FROM
		_TablesScroll
	INTO	@OriginName
	,	@TargetName
	,	@Schema;

	IF	@@FETCH_STATUS != 0	BREAK;

	SET	@Request	= REPLACE( @RequestTemplate	, '@OriginName'	, @OriginName	);
	SET	@Request	= REPLACE( @Request		, '@TargetName'	, @TargetName	);
	SET	@Request	= REPLACE( @Request		, '@Schema'	, @Schema	);

	PRINT	FORMATMESSAGE('Processing %s.%s', @Schema, @OriginName);

	EXEC(@Request);
END;

CLOSE		_TablesScroll;
DEALLOCATE 	_TablesScroll;
GO