DECLARE	
	@Code		TVarCode	,
	@Flag		TFlag		,
	@RepoFlag	TFlag		


SET	@Code		= 'Repozitory'

SELECT	@RepoFlag	= Flag
FROM	GlobalParam
WHERE	OwnerId		= -1
AND	Code		= @Code
AND	Kind		= 0

IF	@@ROWCOUNT = 0	BEGIN

	SELECT	@RepoFlag	= ISNULL( max( Flag ), 0 ) + 1
	FROM	GlobalParam
	WHERE	OwnerId		= -1

	EXEC	dbo.sc_SetGlobalParam -1, @Code, 0, @RepoFlag, 0, '��������� �����������', '', '', 1
END

/*
	��������� ��� ����� N1
*/
SET	@Code		= 'File_N1'

SELECT	@Flag		= Flag
FROM	GlobalParam
WHERE	OwnerId		= @RepoFlag
AND	Code		= @Code
AND	Kind		= @RepoFlag

IF	@@ROWCOUNT = 0	BEGIN

	SELECT	@Flag		= ISNULL( max( Flag ), 0 ) + 1
	FROM	GlobalParam
	WHERE	OwnerId		= @RepoFlag

	EXEC	dbo.sc_SetGlobalParam @RepoFlag, @Code, @RepoFlag, @Flag, 0, '��������� ��� ����� N1', '', '', 1
END

EXEC	dbo.sc_SetGlobalParam @RepoFlag, 'BuildType'	, @Flag,	1,	0,	'������ ��������� �����'				,	''						,'1-������� �����; 2-����� ������������'		,	0x01
EXEC	dbo.sc_SetGlobalParam @RepoFlag, 'DrillDownMode', @Flag,	1,	0,	'���������� ������������ ���������'			,	''						,'����� - ���������� ���� �������� ���������'		,	0x01
EXEC	dbo.sc_SetGlobalParam @RepoFlag, 'DrillDownDays',@Flag	,	360	,0	,'����� �������� ������������ ���������'		,''							,'1 - ������������, 0 - �� ������������'		,0x00
EXEC	dbo.sc_SetGlobalParam @RepoFlag, 'K070List'	, @Flag,	1,	0,	'�������� �070 � ����� N1,N2'				,'14100,14101,14200,14201,14300,14410,14420,14430'	,''							, 0x01
EXEC	dbo.sc_SetGlobalParam @RepoFlag, 'AccountsList'	, @Flag,	0,	0,	'�������� R020 � ����� N1'				,'2233,2433,2453,2431'					,''							, 0x01

GO
