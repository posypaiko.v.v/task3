IF	OBJECT_ID('dbo.sc_SetVersion')	IS NOT NULL
	EXEC	dbo.sc_SetVersion
		@TaskCode	= 'dbo.sv_RepoXMLStoreProt_1C'
	,	@Version	= '1.004 from 24.05.2021'
	,	@Build		= 4
	,	@TaskName	= '���������� ����������� ������'
GO
---------------------------------------------------------------------------------------------------
--	SELECT * FROM sv_RepoXMLStoreProt_1C
---------------------------------------------------------------------------------------------------
IF	OBJECT_ID('dbo.sv_RepoXMLStoreProt_1C')	IS NOT NULL
	DROP	VIEW dbo.sv_RepoXMLStoreProt_1C
GO

CREATE	VIEW	dbo.sv_RepoXMLStoreProt_1C
AS
SELECT	Id
,	TableId
,	[No]
,	EKP
,	D060_1
,	Z230_1
,	Z272	
,	F006	
,	D060_2	
,	Z230_2
,	Q007_1	
,	Q007_2	
,	K025	
,	Q001	
,	M001	
FROM	dbo.RepoXMLStoreProt_1C
GO

GRANT	SELECT
ON	dbo.sv_RepoXMLStoreProt_1C
TO	sc_Servers
GO