--	MATCH_Z330.DBF
EXEC	dbo.ui_SetRefferOption	@RefferCode = 'MATCH_Z330.DBF', @TypeFlag = 5, @Flag = 0
EXEC	dbo.ui_SetRefferList	@RefferCode = 'MATCH_Z330.DBF', @ParamCode = 'Z330', @Flag = 1
EXEC	dbo.ui_SetRefferList	@RefferCode = 'MATCH_Z330.DBF', @ParamCode = 'Z270', @Flag = 2
EXEC	dbo.ui_SetRefferList	@RefferCode = 'MATCH_Z330.DBF', @ParamCode = 'PosData', @Flag = 2
EXEC	dbo.ui_SetRefferList	@RefferCode = 'MATCH_Z330.DBF', @ParamCode = 'OperCode', @Flag = 2
EXEC	dbo.ui_SetRefferList	@RefferCode = 'MATCH_Z330.DBF', @ParamCode = 'ReasonCode', @Flag = 2

GO

