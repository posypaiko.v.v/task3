--	1	- DEVSC-34350	-- ����������
IF	OBJECT_ID('dbo.sc_SetVersion')	IS NOT NULL
	EXEC	dbo.sc_SetVersion
		@TaskCode	= 'dbo.rp_RepoXML_Proc_N2'
	,	@Version	= '1.001 from 10.02.2022'
	,	@Build		= 1
	,	@TaskName	= '���������� ���������� ����� N2'
GO
----------------------------------------------------------------------------------------------------------------------------
--EXEC	rp_RepoXML_Proc_N2	@XMLFileId = 5585, @@DebugLevel = 1, @Treatyid = 8235
----------------------------------------------------------------------------------------------------------------------------
IF	OBJECT_ID( 'dbo.rp_RepoXML_Proc_N2' )	IS NOT NULL
	DROP	PROCEDURE	dbo.rp_RepoXML_Proc_N2
GO

CREATE	PROC	dbo.rp_RepoXML_Proc_N2
	@XMLFileId		TRowId		
,	@DebugLevel 		TFlag	= 0   	-- ���� ������ �������� ����������	
,	@TreatyId		TRowId	= NULL	-- Id ��������
	
AS
BEGIN
SET	NOCOUNT		ON
SET	ROWCOUNT	0
SET	QUERY_GOVERNOR_COST_LIMIT	1000000000
SET	LOCK_TIMEOUT			1200000

--	���������� 
DECLARE
	@t				TDateTime			-- ������� (�����)
,	@Delta				TInt				-- ������� (������)
,	@Issue				TInt
,	@A010				TTag
,	@BankCode			TInt				-- ��� �����
,	@SelfBranchId			TRowId				-- ������������� ������ �������
,	@ReportDate			TDate				-- �������� ����
,	@D1				TDate
,	@D2				TDate
,	@ReportDatePrev			TDate
,	@TableId			TRowId				-- ��������
,	@TLinkId		        TRowId		
--	���������
,	@K070List			TInfo		=	'14100,14101,14200,14201,14300,14410,14420,14430'		-- �������� �070 � ����� N2,N2
--,	@AccountsList			TInfo		=	'2233,2433,2453,2431'						-- �������� R020 � ����� N2
,	@IsUsedCCR			TInt		=	1
--	�������� �� ���������� ������
,	@FileCodeF8			Char(2)		=	'F8'
,	@F8FileId			TRowId				-- ��. ����� F8
,	@F8FileId_2			TRowId				-- ��. ����� F8 �� ���������� 2 �����
,	@F8FileId_3			TRowId				-- ��. ����� F8 �� ���������� 3 �����
,	@FileCodeD51			Char(3)		=	'D51'
,	@D51FileId			TRowId				-- ��. ����� D51
,	@D51FileIdPrevKv		TRowId				-- ��. ����� D51 �� ���������� ������
,	@D51FileIdPrevM			TRowId				-- ��. ����� D51 �� ���������� ������
,	@A010_CCR			Char(3)		=	'CCR'
,	@CCRFileId			TRowId				-- �� ����� CCR
,	@A010_A7			Char(3)		=	'A7'
,	@A7FileId			TRowId				-- �� ����� A7
,	@A7TableId			TRowId				-- �� ��������� ����� A7

--	���������
,	@NameECONOMY_SECTION		TCode		=	'ECONOMY_SECTION'
--	�������
--	����������� �������
CREATE	TABLE	#K070ListN2
(	Id		TInt		NOT NULL	PRIMARY KEY
,	K070		TCode		NOT NULL	UNIQUE
,	K072		TTag		NOT NULL			,
)

CREATE	TABLE	#K070
(	K070		TCode		PRIMARY	KEY
,	K072		TTag		,
)
--	[+]�������� * �����
CREATE	TABLE	#TreatyN2
(	xId				TRowId		IDENTITY
,	TreatyId			TRowId		NOT NULL	-- Id ��������
,	OrgDate				TCode		NULL
,	DateFrom			TCode		NULL
,	DateInto			TCode		NULL
,	R020				TTag		NULL
,	AmountId			TRowId		NOT NULL	-- Id �����
,	ClientId			TRowId		NOT NULL	-- Id �������
,	Confirmed			TMoney		NULL		DEFAULT($0)	--������� �� �����
,	ConfirmedPrevKv			TMoney		NULL		DEFAULT($0)	--������� �� ����� �� ���������� �������� ����
,	ConfirmedPrevM			TMoney		NULL		DEFAULT($0)	--������� �� ����� �� ���������� �������� ����
,	DTurns				TMoney		NULL		DEFAULT($0)	--������ �� �-��-����������
,	DTurns2				TMoney		NULL		DEFAULT($0)	--������ �� �-��-����������
,	DTurns3				TMoney		NULL		DEFAULT($0)	--������ �� �-��-����������
,	CTurns				TMoney		NULL		DEFAULT($0)	--������ �� �-��-����������
,	DTurnsCrncy			TMoney		NULL		DEFAULT($0)	--������ �� �-��-�������
,	DTurnsCrncy2			TMoney		NULL		DEFAULT($0)	--������ �� �-��-�������
,	DTurnsCrncy3			TMoney		NULL		DEFAULT($0)	--������ �� �-��-�������
,	CTurnsCrncy			TMoney		NULL		DEFAULT($0)	--������ �� �-��-�������
,	T100_EKP010203			TMoney		NULL		DEFAULT($0)	--������ �� EKP_AN2001,EKP_AN2002,EKP_AN2003
,	T100_EKP04			TMoney		NULL		DEFAULT($0)	--������ �� EKP_AN2004
,	EKP_05				TInt		NULL
,	EKP_06				TInt		NULL
,	EKP_07				TInt		NULL
,	EKP_08				TInt		NULL
,	EKP_09				TInt		NULL
,	EKP_10				TInt		NULL
,	EKP_11				TInt		NULL
,	InFileCode			TCode		NULL
,	DateDefolt			TDate		NULL
,	R030				TTag		NULL				--�� ��������� F8
,	S260				TTag		NULL				--�� ��������� F8
,	S031				TTag		NULL				--�� ��������� F8
,	S032				TTag		NULL				--�� ��������� F8
,	KU				TTag		NULL				--�� ��������� D51
,	K070				TCode		NULL		DEFAULT('14430')--�� ���������� �������
,	K072				TTag		NULL				--�� ��������� D51		
,	F048				TTag		NULL				--�� ��������� D51
,	F034				TTag		NULL				--�� ��������� F8
,	S130				TTag		NULL		DEFAULT('#')		--������ � CCR
,	DSTI				TTag		NULL		DEFAULT('9')		--�������� ���� � �������� �������
,	DSTI_NUM			TMoney		NULL		DEFAULT(0)	--���� Confirmed �� ��������� �7 ����� � S240 = 1,2,I
,	DSTI_DENOM			TMoney		NULL		DEFAULT(0)	--���� ISNULL(c.ConfirmedIncome,$0)+ISNULL(c.UnconfirmedIncome,$0)  � ������� dbo.ClientCreditRegistryExtensions
,	N140				TTag		NULL		DEFAULT('7')
--,	Share				TMoney		NULL
--,	Refinance			TRowId		NULL				,
)

CREATE	TABLE	#RatesN2
(        CurrencyTag			TTag		
,	IniNum				NUMERIC(38, 19)		NOT NULL
,	IniDen				NUMERIC(38, 19)		NOT NULL
,	R030				TTag			NULL			,
)

CREATE	TABLE	#ProtocolN2
(	xId				TRowId		IDENTITY
,	Line				TBigInt
,	TreatyId			TRowId		NOT NULL	-- Id ��������
,	OrgDate				TCode		NULL
,	DateFrom			TCode		NULL
,	DateInto			TCode		NULL
,	R020				TTag		NULL
,	AmountId			TRowId		NOT NULL	-- Id �����
,	ClientId			TRowId		NOT NULL	-- Id �������
,	Confirmed			TMoney		NULL		DEFAULT($0)	--������� �� �����
,	ConfirmedPrevKv			TMoney		NULL		DEFAULT($0)	--������� �� ����� �� ���������� �������� ����
,	ConfirmedPrevM			TMoney		NULL		DEFAULT($0)	--������� �� ����� �� ���������� �������� ����
,	DTurns				TMoney		NULL		DEFAULT($0)	--������ �� �-��-����������
,	DTurns2				TMoney		NULL		DEFAULT($0)	--������ �� �-��-����������
,	DTurns3				TMoney		NULL		DEFAULT($0)	--������ �� �-��-����������
,	CTurns				TMoney		NULL		DEFAULT($0)	--������ �� �-��-����������
,	DTurnsCrncy			TMoney		NULL		DEFAULT($0)	--������ �� �-��-�������
,	DTurnsCrncy2			TMoney		NULL		DEFAULT($0)	--������ �� �-��-�������
,	DTurnsCrncy3			TMoney		NULL		DEFAULT($0)	--������ �� �-��-�������
,	CTurnsCrncy			TMoney		NULL		DEFAULT($0)	--������ �� �-��-�������
,	T100_EKP010203			TMoney		NULL		DEFAULT($0)	--������ �� EKP_AN2001,EKP_AN2002,EKP_AN2003
,	T100_EKP04			TMoney		NULL		DEFAULT($0)	--������ �� EKP_AN2004
,	EKP_05				TInt		NULL
,	EKP_06				TInt		NULL
,	EKP_07				TInt		NULL
,	EKP_08				TInt		NULL
,	EKP_09				TInt		NULL
,	EKP_10				TInt		NULL
,	EKP_11				TInt		NULL
,	InFileCode			TCode		NULL
,	DateDefolt			TDate		NULL
,	R030				TTag		NULL				--�� ��������� F8
,	S260				TTag		NULL				--�� ��������� F8
,	S031				TTag		NULL				--�� ��������� F8
,	S032				TTag		NULL				--�� ��������� F8
,	KU				TTag		NULL				--�� ��������� D51
,	K070				TCode		NULL		DEFAULT('14430')--�� ���������� �������
,	K072				TTag		NULL				--�� ��������� D51		
,	F048				TTag		NULL				--�� ��������� D51
,	F034				TTag		NULL				--�� ��������� F8
,	S130				TTag		NULL		DEFAULT('#')		--������ � CCR
,	DSTI				TTag		NULL		DEFAULT('9')		--�������� ���� � �������� �������
,	DSTI_NUM			TMoney		NULL		DEFAULT(0)	--���� Confirmed �� ��������� �7 ����� � S240 = 1,2,I
,	DSTI_DENOM			TMoney		NULL		DEFAULT(0)	--���� ISNULL(c.ConfirmedIncome,$0)+ISNULL(c.UnconfirmedIncome,$0)  � ������� dbo.ClientCreditRegistryExtensions
,	N140				TTag		NULL		DEFAULT('7')	,
)
--	������ �� ����� �� ����������
SELECT	TOP 1
	@A010		=	A010
,	@D1		=	DateFrom
,	@D2		=	DateInto
,	@ReportDate	=	ReportDate
FROM	dbo.RepoXMLFiles
WHERE	Id	=	@XMLFileId

IF	@@ERROR	<> 0
	GOTO	ERR_TRAN

SET	@ReportDatePrev	=	dbo.fn_GetPrevWorkDate( @D2 )
--SET	@A010		=	'N2' -- ��� ������������ ���� ��� ����������
---------------------------------------------------------------------------------------------------------
--	������� ��������
SET	@K070List		=	dbo.fn_GetRepoInfo(@A010	,'K070List'		,@K070List		)	
--SET	@AccountsList	=	dbo.fn_GetRepoInfo(@A010	,'AccountsList'		,@AccountsList)
SET	@IsUsedCCR		=	dbo.fn_GetRepoFlag('6B'		,'IsUsedCCR'		,@IsUsedCCR		)
SET	@NameECONOMY_SECTION	=	dbo.fn_GetRepoInfo(@A010	,'ECONOMY_SECTION'	,@NameECONOMY_SECTION	)

IF	@DebugLevel & 0x01 != 0
BEGIN
	SELECT	@K070List			K070List
	,	@NameECONOMY_SECTION		NameECONOMY_SECTION
		
	SELECT	@t	= GETDATE()			,
		@Delta	= DATEDIFF( ms, @t, GETDATE() )	
	RAISERROR( ' * (%8d) RST-������', 0, 1, @Delta ) WITH NOWAIT

	
END

INSERT	INTO	#K070
(	K070
,	K072
)
SELECT	DISTINCT
	K070
,	K072
FROM	xKL_K070
WHERE	@ReportDate-1	BETWEEN DATEB AND DATEE

IF	@@ERROR <> 0
	GOTO	ERR_TRAN

--	���� BranchId	
EXEC	@Issue		= dbo.Sc_GetSelfInfo
	@BankCode	= @BankCode	OUT
,	@BranchId	= @SelfBranchId OUT

IF	@@ERROR	<> 0
OR	@Issue <> 1
	GOTO	ERR_TRAN

SELECT	DISTINCT
	A3		CurrencyTag
,	r030		R030
INTO	#R030
FROM	dbo.xKL_R030
WHERE	R030 NOT IN	('','#','980')
AND 	A3 	NOT IN ('')
AND	@ReportDate-1	BETWEEN DATEB AND DATEE

--	����� �� ������ ��������� ������� - �����, ����� �������� ����� �� �������� � ������

INSERT INTO #RatesN2
(	CurrencyTag
,	IniNum
,	IniDen
,	R030
)
SELECT	r.CurrencyTag
,	IniNum
,	IniDen
,	R030
FROM	Rates		AS	R
LEFT
JOIN	#R030		AS	R030
	ON	R030.CurrencyTag	= r.CurrencyTag
	
WHERE	BranchId	=	dbo.fn_GetBranchId()
AND	DayDate	=	        (       
                                SELECT	Max(DayDate) 
				FROM	Rates 
                                WHERE	DayDate	<=	@D2 
                                AND	ActionId =	0
                                )
AND	R.ActionId	= 0


IF	@@ERROR <> 0
	GOTO	ERR_TRAN
	
--	��� ������� ������ ���� - 1:1.
INSERT INTO #RatesN2
(	CurrencyTag
,	IniNum
,	IniDen
,	R030
)
SELECT '' AS CurrencyTag
,	1
,	1
,	'980'

IF	@@ERROR <> 0
	GOTO	ERR_TRAN

INSERT	INTO	#K070ListN2
(	Id
,	K070
,	K072
)
SELECT	Id
,	Line
,	K072
FROM	dbo.fn_ParseLine(@K070List, ',')	k
JOIN	#K070
	ON	#K070.K070	=	k.Line

IF	@@ERROR <> 0
	GOTO	ERR_TRAN

--	�������� ������  �� D51 ����� �� �������
SELECT	TOP 1
	@D51FileId      = Id
FROM	dbo.RepoXMLFiles	x
WHERE	A010		= @FileCodeD51
AND	ReportDate	= @ReportDate
AND	DateInto	= @D2
AND	InputKind	= 0
AND	CheckState & 0x1 = 0 
AND	@D51FileId	IS NULL
AND	EXISTS		(
			SELECT	1
			FROM	sv_RepoXMLStoreProt_D51	     as p
			JOIN	dbo.sv_RepoXMLTableLinks      as fl
				ON	fl.TableId     = p.TableId
				AND	fl.XMLFileId   = x.Id
			)

IF	@@ERROR <> 0
	GOTO	ERR_TRAN

SELECT	TOP 1
	@D51FileIdPrevKv	=	Id
FROM	dbo.RepoXMLFiles	x
WHERE	A010		= @FileCodeD51
AND	ReportDate	= @D1
AND	DateInto	= @D1-1
AND	InputKind	= 0
AND	CheckState & 0x1 = 0 
AND	@D51FileIdPrevKv IS NULL
AND	EXISTS		(
			SELECT	1
			FROM	sv_RepoXMLStoreProt_D51	     as p
			JOIN	dbo.sv_RepoXMLTableLinks      as fl
				ON	fl.TableId     = p.TableId
				AND	fl.XMLFileId   = x.Id
			)

IF	@@ERROR <> 0
	GOTO	ERR_TRAN

SELECT	TOP 1
	@D51FileIdPrevM	=	Id
FROM	dbo.RepoXMLFiles	x
WHERE	A010		= @FileCodeD51
AND	ReportDate	BETWEEN @D2-31 AND @D2-25
AND	DateInto	= ReportDate - 1
AND	InputKind	= 0
AND	CheckState & 0x1 = 0 
AND	@D51FileIdPrevM IS NULL
AND	EXISTS		(
			SELECT	1
			FROM	sv_RepoXMLStoreProt_D51	     as p
			JOIN	dbo.sv_RepoXMLTableLinks      as fl
				ON	fl.TableId     = p.TableId
				AND	fl.XMLFileId   = x.Id
			)

IF	@@ERROR <> 0
	GOTO	ERR_TRAN
	
INSERT	INTO	#TreatyN2
(	TreatyId	
--,	OrgDate		
,	DateFrom	
,	DateInto
,	R020	
,	AmountId	
,	ClientId	
,	ConFirmed	
,	R030	
,	S260		
,	S031		
,	S032
,	KU
,	K072
,	F048	
,	S130	
)
SELECT	TreatyId	
--,	OrgDate		
,	DateFrom	
,	DateInto
,	R020	
,	AmountId	
,	ClientId	
,	ConFirmed	
,	R030	
,	S260		
,	S031		
,	S032
,	KU
,	K072
,	F048
,	S130	
FROM	dbo.RepoXMLStoreProt_D51		p
JOIN	dbo.sv_RepoXMLTableLinks	fl
	ON	fl.TableId	=	p.TableId
	AND	fl.XMLFileId	=	@D51FileId
WHERE	S010	NOT IN	('3','4','5')
AND	EXISTS	(
		SELECT	1
		FROM	#K070ListN2
		WHERE	#K070ListN2.K072	=	p.K072
		)

IF	@@ERROR <> 0
	GOTO	ERR_TRAN

INSERT	INTO	#TreatyN2
(	TreatyId	
--,	OrgDate		
,	DateFrom	
,	DateInto
,	R020	
,	AmountId	
,	ClientId	
,	ConFirmed
,	ConfirmedPrevKv	
,	R030	
,	S260		
,	S031		
,	S032
,	KU
,	K072
,	F048
,	S130		
)
SELECT	TreatyId	
--,	OrgDate		
,	DateFrom	
,	DateInto
,	R020	
,	AmountId	
,	ClientId	
,	$0
,	Confirmed	
,	R030	
,	S260		
,	S031		
,	S032
,	KU
,	K072
,	F048
,	S130
FROM	dbo.RepoXMLStoreProt_D51		p
JOIN	dbo.sv_RepoXMLTableLinks	fl
	ON	fl.TableId	=	p.TableId
	AND	fl.XMLFileId	=	@D51FileIdPrevKv
WHERE	S010	NOT IN	('3','4','5')
AND	EXISTS	(
		SELECT	1
		FROM	#K070ListN2
		WHERE	#K070ListN2.K072	=	p.K072
		)
AND	NOT EXISTS
		(
		SELECT	1
		FROM	#TreatyN2
		WHERE	#TreatyN2.TreatyId	=	p.TreatyId
		)

IF	@@ERROR <> 0
	GOTO	ERR_TRAN

UPDATE	t
SET	ConfirmedPrevKv	=	p.Confirmed	
FROM	#TreatyN2	t
JOIN	dbo.RepoXMLStoreProt_D51		p
	ON	p.TreatyId	=	t.TreatyId
	AND	p.AmountId	=	t.AmountId
JOIN	dbo.sv_RepoXMLTableLinks	fl
	ON	fl.TableId	=	p.TableId
	AND	fl.XMLFileId	=	@D51FileIdPrevKv
WHERE	p.S010	NOT IN	('3','4','5')
AND	EXISTS	(
		SELECT	1
		FROM	#K070ListN2
		WHERE	#K070ListN2.K072	=	p.K072
		)

IF	@@ERROR <> 0
	GOTO	ERR_TRAN

INSERT	INTO	#TreatyN2
(	TreatyId	
--,	OrgDate		
,	DateFrom	
,	DateInto
,	R020	
,	AmountId	
,	ClientId	
,	ConfirmedPrevM	
,	R030	
,	S260		
,	S031		
,	S032
,	KU
,	K072
,	F048
,	S130		
)
SELECT	TreatyId	
--,	OrgDate		
,	DateFrom	
,	DateInto
,	R020	
,	AmountId	
,	ClientId	
,	Confirmed	
,	R030	
,	S260		
,	S031		
,	S032
,	KU
,	K072
,	F048
,	S130	
FROM	dbo.RepoXMLStoreProt_D51		p
JOIN	dbo.sv_RepoXMLTableLinks	fl
	ON	fl.TableId	=	p.TableId
	AND	fl.XMLFileId	=	@D51FileIdPrevM
WHERE	S010	NOT IN	('3','4','5')
AND	EXISTS	(
		SELECT	1
		FROM	#K070ListN2
		WHERE	#K070ListN2.K072	=	p.K072
		)
AND	NOT EXISTS
		(
		SELECT	1
		FROM	#TreatyN2
		WHERE	#TreatyN2.TreatyId	=	p.TreatyId
		)

IF	@@ERROR <> 0
	GOTO	ERR_TRAN

UPDATE	t
SET	ConfirmedPrevM	=	p.Confirmed	
FROM	#TreatyN2	t
JOIN	dbo.RepoXMLStoreProt_D51		p
	ON	p.TreatyId	=	t.TreatyId
	AND	p.AmountId	=	t.AmountId
JOIN	dbo.sv_RepoXMLTableLinks	fl
	ON	fl.TableId	=	p.TableId
	AND	fl.XMLFileId	=	@D51FileIdPrevM
WHERE	p.S010	NOT IN	('3','4','5')
AND	EXISTS	(
		SELECT	1
		FROM	#K070ListN2
		WHERE	#K070ListN2.K072	=	p.K072
		)

IF	@@ERROR <> 0
	GOTO	ERR_TRAN

IF	@DebugLevel & 0x01 != 0
BEGIN
	SELECT	@D51FileId	D51FileId, @D51FileIdPrevKv	D51FileIdPrevKv, @D51FileIdPrevM D51FileIdPrevM 
	SELECT	TOP 1000 'N2_1',* FROM #TreatyN2 WHERE TreatyId = @TreatyId
	SELECT	'CNTN2_1', COUNT(TreatyId) FROM  #TreatyN2

	SELECT  @Delta	= DATEDIFF( ms, @t, GETDATE() )	,
		@t	= GETDATE()
	RAISERROR( ' * (%8d) N2-��������D51.', 0, 1, @Delta ) WITH NOWAIT
END

--	�������� ������  �� F8 ����� �� �������
SELECT	TOP 1
	@F8FileId      = Id
FROM	dbo.RepoXMLFiles	x
WHERE	A010		= @FileCodeF8
AND	ReportDate	= @ReportDate
AND	DateInto	= @D2
AND	InputKind	= 0
AND	CheckState & 0x1 = 0 
AND	@F8FileId	IS NULL
AND	EXISTS		(
			SELECT	1
			FROM	sv_RepoXMLStoreProt_F8	     as p
			JOIN	dbo.sv_RepoXMLTableLinks      as fl
				ON	fl.TableId     = p.TableId
				AND	fl.XMLFileId   = x.Id
			)

IF	@@ERROR <> 0
	GOTO	ERR_TRAN

SELECT	TOP 1
	@F8FileId_2      = Id
FROM	dbo.RepoXMLFiles	x
WHERE	A010		= @FileCodeF8
AND	ReportDate	BETWEEN @D2-31 AND @D2-25
AND	DateInto	= ReportDate - 1
AND	InputKind	= 0
AND	CheckState & 0x1 = 0 
AND	@F8FileId_2	IS NULL
AND	EXISTS		(
			SELECT	1
			FROM	sv_RepoXMLStoreProt_F8	     as p
			JOIN	dbo.sv_RepoXMLTableLinks      as fl
				ON	fl.TableId     = p.TableId
				AND	fl.XMLFileId   = x.Id
			)

IF	@@ERROR <> 0
	GOTO	ERR_TRAN

SELECT	TOP 1
	@F8FileId_3      = Id
FROM	dbo.RepoXMLFiles	x
WHERE	A010		= @FileCodeF8
AND	ReportDate	BETWEEN @D2-61 AND @D2-50
AND	DateInto	= ReportDate - 1
AND	InputKind	= 0
AND	CheckState & 0x1 = 0 
AND	@F8FileId_3	IS NULL
AND	EXISTS		(
			SELECT	1
			FROM	sv_RepoXMLStoreProt_F8	     as p
			JOIN	dbo.sv_RepoXMLTableLinks      as fl
				ON	fl.TableId     = p.TableId
				AND	fl.XMLFileId   = x.Id
			)

IF	@@ERROR <> 0
	GOTO	ERR_TRAN

UPDATE	t
SET	DTurns		=	p.DTurns
,	DTurnsCrncy	=	p.DTurnsCrncy
,	CTurns		=	p.CTurns
,	CTurnsCrncy	=	p.CTurnsCrncy
FROM	#TreatyN2			t	
JOIN	(
	SELECT	TreatyId
	,	AccountId
	,	ClientId
	,	SUM(DTurns)		DTurns
	,	SUM(DTurnsCrncy)	DTurnsCrncy
	,	SUM(CTurns)		CTurns
	,	SUM(CTurnsCrncy)	CTurnsCrncy
	FROM	dbo.RepoXMLStoreProt_F8
	JOIN	dbo.sv_RepoXMLTableLinks	fl
		ON	fl.TableId	=	RepoXMLStoreProt_F8.TableId
		AND	fl.XMLFileId	=	@F8FileId
	WHERE	F034	IN	('21','22','23')
	GROUP BY
		TreatyId
	,	AccountId
	,	ClientId
	)	p
	ON	t.TreatyId	=	p.TreatyId
	AND	t.AmountId	=	p.AccountId
	AND	t.ClientId	=	p.ClientId
WHERE	EXISTS	(
		SELECT	1
		FROM	#TreatyN2
		WHERE	#TreatyN2.TreatyId	=	p.TreatyId
		)

IF	@@ERROR <> 0
	GOTO	ERR_TRAN

UPDATE	t
SET	DTurns2		=	p.DTurns
,	DTurnsCrncy2	=	p.DTurnsCrncy
FROM	#TreatyN2	t
JOIN	(
	SELECT	TreatyId
	,	AccountId
	,	ClientId
	,	SUM(DTurns)		DTurns
	,	SUM(DTurnsCrncy)	DTurnsCrncy
	,	SUM(CTurns)		CTurns
	,	SUM(CTurnsCrncy)	CTurnsCrncy
	FROM	dbo.RepoXMLStoreProt_F8
	JOIN	dbo.sv_RepoXMLTableLinks	fl
		ON	fl.TableId	=	RepoXMLStoreProt_F8.TableId
		AND	fl.XMLFileId	=	@F8FileId_2
	WHERE	F034	IN	('21','22','23')
	GROUP BY
		TreatyId
	,	AccountId
	,	ClientId
	)	p
	ON	t.TreatyId	=	p.TreatyId
	AND	t.AmountId	=	p.AccountId
	AND	t.ClientId	=	p.ClientId	
WHERE	EXISTS	(
		SELECT	1
		FROM	#TreatyN2
		WHERE	#TreatyN2.TreatyId	=	p.TreatyId
		)

IF	@@ERROR <> 0
	GOTO	ERR_TRAN

UPDATE	t
SET	DTurns3		=	p.DTurns
,	DTurnsCrncy3	=	p.DTurnsCrncy
FROM	#TreatyN2			t	
JOIN	(
	SELECT	TreatyId
	,	AccountId
	,	ClientId
	,	SUM(DTurns)		DTurns
	,	SUM(DTurnsCrncy)	DTurnsCrncy
	,	SUM(CTurns)		CTurns
	,	SUM(CTurnsCrncy)	CTurnsCrncy
	FROM	dbo.RepoXMLStoreProt_F8
	JOIN	dbo.sv_RepoXMLTableLinks	fl
		ON	fl.TableId	=	RepoXMLStoreProt_F8.TableId
		AND	fl.XMLFileId	=	@F8FileId_3
	WHERE	F034	IN	('21','22','23')
	GROUP BY
		TreatyId
	,	AccountId
	,	ClientId
	)	p
	ON	t.TreatyId	=	p.TreatyId
	AND	t.AmountId	=	p.AccountId
	AND	t.ClientId	=	p.ClientId
WHERE	EXISTS	(
		SELECT	1
		FROM	#TreatyN2
		WHERE	#TreatyN2.TreatyId	=	p.TreatyId
		)

IF	@@ERROR <> 0
	GOTO	ERR_TRAN

IF	@DebugLevel & 0x01 != 0
BEGIN
	SELECT	@F8FileId	F8FileId, 	@F8FileId_2	F8FileId_2,	@F8FileId_3	F8FileId_3
	SELECT	'N2_1',* FROM #TreatyN2 WHERE TreatyId = @TreatyId
	SELECT	'CNTN2_1', COUNT(TreatyId) FROM  #TreatyN2
	
	SELECT  @Delta	= DATEDIFF( ms, @t, GETDATE() )	,
		@t	= GETDATE()
	RAISERROR( ' * (%8d) N2-��������F8.', 0, 1, @Delta ) WITH NOWAIT
END

UPDATE	t
SET	InFileCode	=	r.InFileCode	
FROM	#TreatyN2	t
JOIN	dbo.RepoTopics	r
	ON	t.R020		=	r.TopicId
	AND	r.Slice		=	'D51X'
	AND	@ReportDate	BETWEEN r.DateFrom AND r.DateInto
	AND	[Status]	= 1 

--	������ ������. �������
IF	@IsUsedCCR	>	0
BEGIN
	--	������ CCR
	EXEC	dbo.sc_RepoXMLGetServiceFileIdByRD
		@A010		= @A010_CCR	,
		@ReportDate	= @ReportDate	,
		@XMLFileId	= @CCRFileId	OUT

	IF	@@ERROR <> 0
	OR	@Issue  <> 1
		GOTO	ERR_LOAD_CCR

	UPDATE	t
	SET	DateDefolt	=	p.DateDefolt
	FROM	#TreatyN2	t
	JOIN	(				
		SELECT	TreatyId		
		,	ClientId		
		,	MIN(DateDefolt)	DateDefolt
		FROM	dbo.RepoXMLStoreData_CCR
		WHERE	DateDefolt	IS	NOT NULL
		AND	IsLimit		=	0
		GROUP BY
			TreatyId		
		,	ClientId
		)		p
		ON	t.TreatyId	=	p.TreatyId
		AND	t.ClientId	=	p.ClientId
	

	IF	@@ERROR	<> 0	
		GOTO	ERR_LOAD_CCR
END
--	DSTI ������
UPDATE	t
SET	DSTI_DENOM	=	ISNULL(c.ConfirmedIncome,$0)+ISNULL(c.UnconfirmedIncome,$0) 
FROM	#TreatyN2				t
JOIN	dbo.ClientCreditRegistryExtensions	c
	ON	t.ClientId	=	c.ClientId
	AND	c.Daydate	<=	@D2
	AND	ISNULL(c.ConfirmedIncome,$0)+ISNULL(c.UnconfirmedIncome,$0)	<>	$0
WHERE	NOT EXISTS	(
			SELECT	1
			FROM	ClientCreditRegistryExtensions  c1
			WHERE	c1.ClientId	=	c.ClientId	
			AND	c1.Daydate	<=	@D2	
			AND	c1.Daydate	>	c.DayDate		
			AND	ISNULL(c1.ConfirmedIncome,$0)+ISNULL(c1.UnconfirmedIncome,$0)	<>	$0
			)

IF	@@ERROR	<> 0
	GOTO	ERR_TRAN
--	N140 ������
UPDATE	t
SET	N140	=	CASE	WHEN	DSTI_DENOM	BETWEEN 0		AND	7000.00		THEN	'1'
				WHEN	DSTI_DENOM	BETWEEN 7000.01		AND	12000.00	THEN	'2'
				WHEN	DSTI_DENOM	BETWEEN 12000.01	AND	20000.00	THEN	'3'
				WHEN	DSTI_DENOM	BETWEEN 20000.01	AND	50000.00	THEN	'4'
				WHEN	DSTI_DENOM	BETWEEN 50000.01	AND	100000.00	THEN	'5'
				WHEN	DSTI_DENOM	> 100000					THEN	'6'
				ELSE	'7'
			END
FROM	#TreatyN2				t
WHERE	DSTI_DENOM	<>	$0

IF	@@ERROR	<> 0
	GOTO	ERR_TRAN



SELECT	TOP 1
	@A7FileId      = Id
FROM	dbo.RepoXMLFiles	x
WHERE	A010		= @A010_A7
AND	YEAR(ReportDate)> YEAR(@D2)+1
AND	DateInto	= @ReportDatePrev
AND	InputKind	= 0
AND	CheckState & 0x1 = 0 
AND	@A7FileId	IS NULL
AND	EXISTS		(
			SELECT	1
			FROM	sv_RepoXMLStoreProt_A7	     as p
			JOIN	dbo.sv_RepoXMLTableLinks      as fl
				ON	fl.TableId     = p.TableId
				AND	fl.XMLFileId   = x.Id
			)

IF	@@ERROR	<> 0
	GOTO	ERR_TRAN

SELECT	TOP 1
	@A7FileId      = Id
FROM	dbo.RepoXMLFiles	x
WHERE	A010		= @A010_A7
AND	ReportDate	= @ReportDate
AND	DateInto	= @D2
AND	InputKind	= 0
AND	CheckState & 0x1 = 0 
AND	@A7FileId	IS NULL
AND	EXISTS		(
			SELECT	1
			FROM	sv_RepoXMLStoreProt_A7	     as p
			JOIN	dbo.sv_RepoXMLTableLinks      as fl
				ON	fl.TableId     = p.TableId
				AND	fl.XMLFileId   = x.Id
			)

IF	@@ERROR	<> 0
OR	@A7FileId      IS NULL
	GOTO	ERR_FILEA7
	

SELECT	TOP 1
	@A7TableId      = p.TableId
FROM	sv_RepoXMLStoreProt_A7	     as p
JOIN	dbo.sv_RepoXMLTableLinks      as fl
	ON	fl.TableId     = p.TableId
	AND	fl.XMLFileId   = @A7FileId

IF	@@ERROR <> 0
	GOTO	ERR_TRAN

UPDATE	t
SET	DSTI_NUM	=	tA7.MainAmount
FROM	#TreatyN2				t
JOIN	(
	SELECT	ClientId
	,	SUM(MainAmount)	 MainAmount
	FROM	sv_RepoXMLStoreProt_A7	p
	JOIN	xKL_S240		s
		ON	p.S240		=	s.S240
		AND	s.D_CLOSE	=	''
		AND	p.S240		IN	('1','2','I')
		AND	p.SystemId	IN	('5000','2000','6000')
		AND	p.Activity	=	'1'
	WHERE	TableId			=	@A7TableId 
	AND	EXISTS	(
			SELECT	1
			FROM	#TreatyN2
			WHERE	#TreatyN2.ClientId	=	p.ClientId
			)
	GROUP BY
		ClientId
	)					tA7
	ON	t.ClientId	=	tA7.ClientId

IF	@@ERROR <> 0
	GOTO	ERR_TRAN

UPDATE	t
SET	DSTI	=	CASE	WHEN	1.0 * DSTI_NUM / DSTI_DENOM	BETWEEN 0	AND	0.2000	THEN	'1'
				WHEN	1.0 * DSTI_NUM / DSTI_DENOM	BETWEEN 0.2001	AND	0.3000	THEN	'2'
				WHEN	1.0 * DSTI_NUM / DSTI_DENOM	BETWEEN 0.3001	AND	0.4000	THEN	'3'
				WHEN	1.0 * DSTI_NUM / DSTI_DENOM	BETWEEN 0.4001	AND	0.5000	THEN	'4'
				WHEN	1.0 * DSTI_NUM / DSTI_DENOM	BETWEEN 0.5001	AND	0.6000	THEN	'5'
				WHEN	1.0 * DSTI_NUM / DSTI_DENOM	BETWEEN 0.6001	AND	0.7000	THEN	'6'
				WHEN	1.0 * DSTI_NUM / DSTI_DENOM	BETWEEN 0.7001	AND	0.8000	THEN	'7'
				WHEN	1.0 * DSTI_NUM / DSTI_DENOM	>	0.8001			THEN	'8'
				ELSE	'9'
			END	
,	T100_EKP04	=	100.0 * DSTI_NUM / DSTI_DENOM 
FROM	#TreatyN2	t
WHERE	DSTI_DENOM	<>	$0

UPDATE	t	-- ������ T100_EKP0103 --EKP 03=02+01
SET	T100_EKP010203	=	CASE	WHEN	(Confirmed - ConfirmedPrevM) > $0	
					THEN	Confirmed - ConfirmedPrevM
					ELSE	$0
				END
FROM	#TreatyN2			t
WHERE	S130		IN	('40','42','43','45')	
AND	InFileCode	IN	('','OV','PRS')	

IF	@@ERROR <> 0
	GOTO	ERR_TRAN

UPDATE	t	-- ������ T100_EKP0203 --EKP 03=02+01
SET	T100_EKP010203	=	CASE	WHEN	CTurns			> $0	
					THEN	CTurns
					ELSE	$0
				END
FROM	#TreatyN2			t
WHERE	S130		=	'4Y'
AND	InFileCode	IN	('','OV','PRS')	

IF	@@ERROR <> 0
	GOTO	ERR_TRAN

UPDATE	t	-- ������ EKP_05-EKP_08
SET	EKP_05	=	1
,	EKP_06	=	CASE	WHEN	DateDefolt	IS NOT NULL
				THEN	1
			END
,	EKP_07 =	CASE	WHEN	S130		IN	('40','41','42','43','44','45')	
				THEN	1
			END
,	EKP_08 =	CASE	WHEN	S130		IN	('41','44')	
				THEN	1
			END
FROM	#TreatyN2			t
WHERE	InFileCode	IN	('','OV','PRS')	
AND	Confirmed	<>	$0

IF	@@ERROR <> 0
	GOTO	ERR_TRAN

UPDATE	t	-- ������ EKP_09-EKP_11
SET	EKP_09	=	CASE	WHEN	S130		=	'4Y'
				THEN	1
			END
,	EKP_10 =	CASE	WHEN	S130		IN	('40','41','42','43','44','45')	
				THEN	1
			END
,	EKP_11 =	CASE	WHEN	S130		IN	('41','44')	
				THEN	1
			END
FROM	#TreatyN2			t
WHERE	InFileCode	IN	('','OV')	
AND	DTurns + DTurns2 + DTurns3	<>	$0

IF	@@ERROR <> 0
	GOTO	ERR_TRAN


IF	@DebugLevel & 0x01 != 0
BEGIN
	SELECT	@CCRFileId	CCRFileId
	
	SELECT	'N2_3',* FROM #TreatyN2 WHERE TreatyId = @TreatyId

	SELECT	'N2_4',* FROM #TreatyN2 WHERE EKP_11 = 1
	
	SELECT  @Delta	= DATEDIFF( ms, @t, GETDATE() )	,
		@t	= GETDATE()
	RAISERROR( ' * (%8d) N2-��������DSTI.', 0, 1, @Delta ) WITH NOWAIT
END

UPDATE	t
SET	K070		=	x3.VCode
FROM	#TreatyN2			AS t
JOIN	sv_RepoParams			AS x3
	ON	x3.OwnerId	=	t.ClientId
	AND	x3.TaskCode	=	'Clients'
	AND	x3.ParamCode	=	@NameECONOMY_SECTION
	AND	x3.VCode	IS	NOT NULL

IF	@DebugLevel & 0x01 != 0
BEGIN
	SELECT	'N2_5',* FROM #TreatyN2 WHERE TreatyId = @TreatyId
	SELECT  @Delta	= DATEDIFF( ms, @t, GETDATE() )	,
		@t	= GETDATE()
	RAISERROR( ' * (%8d) N2-��������K070.', 0, 1, @Delta ) WITH NOWAIT
END

INSERT	INTO	#ProtocolN2
(	TreatyId		
,	OrgDate			
,	DateFrom		
,	DateInto		
,	R020			
,	AmountId		
,	ClientId		
,	Confirmed		
,	ConfirmedPrevKv		
,	ConfirmedPrevM		
,	DTurns			
,	DTurns2			
,	DTurns3			
,	CTurns			
,	DTurnsCrncy		
,	DTurnsCrncy2		
,	DTurnsCrncy3		
,	CTurnsCrncy		
,	T100_EKP010203		
,	T100_EKP04		
,	EKP_05			
,	EKP_06			
,	EKP_07			
,	EKP_08			
,	EKP_09			
,	EKP_10			
,	EKP_11			
,	InFileCode		
,	DateDefolt		
,	R030			
,	S260			
,	S031			
,	S032			
,	KU			
,	K070			
,	K072			
,	F048			
,	F034			
,	S130			
,	DSTI			
,	DSTI_NUM		
,	DSTI_DENOM		
,	N140			
 )
SELECT	TreatyId		
,	OrgDate			
,	DateFrom		
,	DateInto		
,	R020			
,	AmountId		
,	ClientId		
,	Confirmed		
,	ConfirmedPrevKv		
,	ConfirmedPrevM		
,	DTurns			
,	DTurns2			
,	DTurns3			
,	CTurns			
,	DTurnsCrncy		
,	DTurnsCrncy2		
,	DTurnsCrncy3		
,	CTurnsCrncy		
,	T100_EKP010203		
,	T100_EKP04		
,	EKP_05			
,	EKP_06			
,	EKP_07			
,	EKP_08			
,	EKP_09			
,	EKP_10			
,	EKP_11			
,	InFileCode		
,	DateDefolt		
,	R030			
,	S260			
,	S031			
,	S032			
,	KU			
,	K070			
,	K072			
,	F048			
,	F034			
,	S130			
,	DSTI			
,	DSTI_NUM		
,	DSTI_DENOM		
,	N140
FROM	#TreatyN2


--	���������� ����� ������
UPDATE	x
SET	Line	=	y.Line
FROM	#ProtocolN2	x
JOIN	(
	SELECT	xId
	,     ROW_NUMBER() OVER (ORDER BY TreatyId, KU)	Line
	FROM	#ProtocolN2
	)		y
ON	y.xId	= x.xId
	
IF	@@ERROR <> 0
	GOTO	ERR_TRAN

--	��������
IF	@XMLFileId	IS NOT	NULL
BEGIN
--	��������
--	������������� ������� ����� �������
	EXEC sc_RepoXML_InitTableColumn	'TreatyId'			,'I� ���.'			,'TRowId'	,'Treaty'
	EXEC sc_RepoXML_InitTableColumn 'OrgDate'			,'���� �i�'			,'TDate'		,NULL
	EXEC sc_RepoXML_InitTableColumn 'DateFrom'			,'���� �������'			,'TDate'	,NULL
	EXEC sc_RepoXML_InitTableColumn 'DateInto'			,'���� ���i������'		,'TDate'	,NULL
	EXEC sc_RepoXML_InitTableColumn 'R020'				,'R020'				,'TString'	,NULL
	EXEC sc_RepoXML_InitTableColumn 'AmountId'			,'I� �������'			,'TRowId'	,'Amounts'
	EXEC sc_RepoXML_InitTableColumn 'ClientId'			,'I� ��.'			,'TRowId'	,'Clients'
	EXEC sc_RepoXML_InitTableColumn 'Confirmed'			,'���� �������, ��.'		,'TMoney'	,NULL
	EXEC sc_RepoXML_InitTableColumn 'ConfirmedPrevKv'		,'���� �������, �����.'		,'TMoney'	,NULL
	EXEC sc_RepoXML_InitTableColumn 'ConfirmedPrevM'		,'���� �������, �ic.'		,'TMoney'	,NULL
	EXEC sc_RepoXML_InitTableColumn 'DTurns'			,'�-� ������, ���.'		,'TMoney'	,NULL
	EXEC sc_RepoXML_InitTableColumn	'DTurns2'			,'�-� ������2, ���.'		,'TMoney'	,NULL
	EXEC sc_RepoXML_InitTableColumn 'DTurns3'			,'�-� ������3, ���.'		,'TMoney'	,NULL
	EXEC sc_RepoXML_InitTableColumn 'CTurns'			,'�-� ������, ���.'		,'TMoney'	,NULL
	EXEC sc_RepoXML_InitTableColumn 'DTurnsCrncy'			,'�-� ������, ���.'		,'TMoney'	,NULL
	EXEC sc_RepoXML_InitTableColumn 'DTurnsCrncy2'			,'�-� ������1, ���.'		,'TMoney'	,NULL
	EXEC sc_RepoXML_InitTableColumn 'DTurnsCrncy3'			,'�-� ������2, ���.'		,'TMoney'	,NULL
	EXEC sc_RepoXML_InitTableColumn 'CTurnsCrncy'			,'�-� ������, ���.'		,'TMoney'	,NULL
	EXEC sc_RepoXML_InitTableColumn 'T100_EKP010203'		,'T100_EKP010203'		,'TMoney'	,NULL
	EXEC sc_RepoXML_InitTableColumn 'T100_EKP04'			,'T100_EKP04'			,'TMoney'	,NULL
	EXEC sc_RepoXML_InitTableColumn 'EKP_05'			,'EKP_05'			,'TRowId'	,NULL
	EXEC sc_RepoXML_InitTableColumn 'EKP_06'			,'EKP_06'			,'TRowId'	,NULL
	EXEC sc_RepoXML_InitTableColumn	'EKP_07'			,'EKP_07'			,'TRowId'	,NULL
	EXEC sc_RepoXML_InitTableColumn 'EKP_08'			,'EKP_08'			,'TRowId'	,NULL
	EXEC sc_RepoXML_InitTableColumn 'EKP_09'			,'EKP_09'			,'TRowId'	,NULL
	EXEC sc_RepoXML_InitTableColumn 'EKP_10'			,'EKP_10'			,'TRowId'	,NULL
	EXEC sc_RepoXML_InitTableColumn 'EKP_11'			,'EKP_11'			,'TRowId'	,NULL
	EXEC sc_RepoXML_InitTableColumn 'InFileCode'			,'InFileCode'			,'TString'	,NULL
	EXEC sc_RepoXML_InitTableColumn 'DateDefolt'			,'DateDefolt'			,'TDate'	,NULL
	EXEC sc_RepoXML_InitTableColumn 'R030'				,'R030'				,'TString'	,NULL
	EXEC sc_RepoXML_InitTableColumn 'S260'				,'S260	'			,'TString'	,NULL
	EXEC sc_RepoXML_InitTableColumn 'S031'				,'S031'				,'TString'	,NULL
	EXEC sc_RepoXML_InitTableColumn 'S032'				,'S032'				,'TString'	,NULL
	EXEC sc_RepoXML_InitTableColumn	'KU'				,'KU'				,'TString'	,NULL
	EXEC sc_RepoXML_InitTableColumn 'K070'				,'K070'				,'TString'	,NULL
	EXEC sc_RepoXML_InitTableColumn 'K072'				,'K072'				,'TString'	,NULL
	EXEC sc_RepoXML_InitTableColumn 'F048'				,'F048'				,'TString'	,NULL
	EXEC sc_RepoXML_InitTableColumn 'F034'				,'F034'				,'TString'	,NULL
	EXEC sc_RepoXML_InitTableColumn 'S130'				,'S130'				,'TString'	,NULL
	EXEC sc_RepoXML_InitTableColumn 'DSTI'				,'DSTI'				,'TString'	,NULL
	EXEC sc_RepoXML_InitTableColumn 'DSTI_NUM'			,'DSTI_NUM'			,'TMoney'	,NULL
	EXEC sc_RepoXML_InitTableColumn 'DSTI_DENOM'			,'DSTI_DENOM'			,'TMoney'	,NULL
	EXEC sc_RepoXML_InitTableColumn 'N140'				,'N140'				,'TString'	,NULL
		
	--	������������� ����� �������
	EXEC	sc_RepoXML_InitTable	'�������� N2'	,DEFAULT	,'sv_RepoXMLStoreProt_N2'	,@TableId	OUT
	--	������������� �������
	EXEC	sc_RepoXML_InitTableLink @XMLFileId	,@TableId	,DEFAULT	,DEFAULT	,@TLinkId	OUT
	--	�������� �������
	--EXEC	sc_RepoXML_Clear_Table	@TableId

	DELETE	FROM	dbo.RepoXMLStoreData_N2
	WHERE	XMLFileId = @XMLFileId
	
	IF	@@ERROR <> 0
		GOTO	ERR_TRAN
	--	������� ������
	INSERT	INTO	dbo.RepoXMLStoreData_N2
	(	TableId
	,	[No]
	,	XMLFileId

	,	TreatyId		
	,	OrgDate			
	,	DateFrom		
	,	DateInto		
	,	R020			
	,	AmountId		
	,	ClientId		
	,	Confirmed		
	,	ConfirmedPrevKv		
	,	ConfirmedPrevM		
	,	DTurns			
	,	DTurns2			
	,	DTurns3			
	,	CTurns			
	,	DTurnsCrncy		
	,	DTurnsCrncy2		
	,	DTurnsCrncy3		
	,	CTurnsCrncy		
	,	T100_EKP010203		
	,	T100_EKP04		
	,	EKP_05			
	,	EKP_06			
	,	EKP_07			
	,	EKP_08			
	,	EKP_09			
	,	EKP_10			
	,	EKP_11			
	,	InFileCode		
	,	DateDefolt		
	,	R030			
	,	S260			
	,	S031			
	,	S032			
	,	KU			
	,	K070			
	,	K072			
	,	F048			
	,	F034			
	,	S130			
	,	DSTI			
	,	DSTI_NUM		
	,	DSTI_DENOM		
	,	N140
 	)
	SELECT	@TableId
	,	[Line]
	,	@XMLFileId
	----------------
	,	TreatyId		
	,	OrgDate			
	,	DateFrom		
	,	DateInto		
	,	R020			
	,	AmountId		
	,	ClientId		
	,	Confirmed		
	,	ConfirmedPrevKv		
	,	ConfirmedPrevM		
	,	DTurns			
	,	DTurns2			
	,	DTurns3			
	,	CTurns			
	,	DTurnsCrncy		
	,	DTurnsCrncy2		
	,	DTurnsCrncy3		
	,	CTurnsCrncy		
	,	T100_EKP010203		
	,	T100_EKP04		
	,	EKP_05			
	,	EKP_06			
	,	EKP_07			
	,	EKP_08			
	,	EKP_09			
	,	EKP_10			
	,	EKP_11			
	,	InFileCode		
	,	DateDefolt		
	,	R030			
	,	S260			
	,	S031			
	,	S032			
	,	KU			
	,	K070			
	,	K072			
	,	F048			
	,	F034			
	,	S130			
	,	DSTI			
	,	DSTI_NUM		
	,	DSTI_DENOM		
	,	N140
 	FROM	#ProtocolN2	p

END
END

EXITSTAT:
	RETURN	1

ERR_RIGHTS_REPORT:
	RAISERROR( '* ������ ������������ ������������ ������', 16, -1 )
	GOTO	ERR_TRAN

ERR_DATEINTO:
	RAISERROR( '* ������ ������������. ��� ������ ������� �� �������� ������ �� ���������� ��� � ������������� ����', 16, -1 )
	GOTO	ERR_TRAN

ERR_ADD:
	RAISERROR( '* ������ ������� �����', 16, 1 )
	GOTO	ERR_TRAN

ERR_TRAN:
	RAISERROR( '* ������ ���������� ������!', 16, -1 )
	RETURN	0

ERR_R030:
	RAISERROR( '* ������ ������ �� ������������ [xKL_R030]', 16, 1 )
	GOTO	ERR_TRAN

ERR_AmountsKind:
	RAISERROR( '* �� ���� ����� ��� ������� ''Amounts''', 16, 1 )
	GOTO	ERR_TRAN

ERR_ClientsKind:
	RAISERROR( '* �� ���� ����� ��� ������� ''Clients''', 16, 1 )
	GOTO	ERR_TRAN
		
ERR_TreatyKind:
	RAISERROR( '* �� ���� ����� ��� ������� ''Treaty''', 16, 1)
	GOTO	ERR_TRAN

ERR_LOAD_CCR:
	RAISERROR( '* ������ �������� �� "CCR" �� ���� [%s]', 16, -1 )
	GOTO	ERR_TRAN

ERR_FILEA7:
	RAISERROR( '* ������ �������� ��������� �7 ����� � ������ ����. ��������', 16, 1 )
	RETURN	0

GO

GRANT	EXECUTE
ON	dbo.rp_RepoXML_Proc_N2
TO	SC_Servers
GO