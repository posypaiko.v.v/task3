IF	NOT EXISTS 
(	SELECT 1 
	FROM SysColumns c WITH (NOLOCK)
	JOIN SysObjects o WITH (NOLOCK) 
		ON 	c.Id 	= o.Id 
		AND 	o.Type 	= 'U' 
		AND 	o.Name 	= 'RepoXMLStoreProt_1C' 
		AND	c.Name 	= 'D060_1'	
)
BEGIN
	ALTER	TABLE	RepoXMLStoreProt_1C
	ADD	D060_1		varchar(2)		NULL
END
GO

IF	NOT EXISTS 
(	SELECT 1 
	FROM SysColumns c WITH (NOLOCK)
	JOIN SysObjects o WITH (NOLOCK) 
		ON 	c.Id 	= o.Id 
		AND 	o.Type 	= 'U' 
		AND 	o.Name 	= 'RepoXMLStoreProt_1C' 
		AND	c.Name 	= 'D060_2'	
)
BEGIN
	ALTER	TABLE	RepoXMLStoreProt_1C
	ADD	D060_2		varchar(2)		NULL
END
GO

