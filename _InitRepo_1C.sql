DECLARE	
	@Code		TVarCode	,
	@Flag		TFlag		,
	@RepoFlag	TFlag		


SET	@Code		= 'Repozitory'

SELECT	@RepoFlag	= Flag
FROM	GlobalParam
WHERE	OwnerId		= -1
AND	Code		= @Code
AND	Kind		= 0

IF	@@ROWCOUNT = 0	BEGIN

	SELECT	@RepoFlag	= ISNULL( max( Flag ), 0 ) + 1
	FROM	GlobalParam
	WHERE	OwnerId		= -1

	EXEC	dbo.sc_SetGlobalParam -1, @Code, 0, @RepoFlag, 0, '��������� �����������', '', '', 1
END

/*
	��������� ��� ����� 1C
*/
SET	@Code		= 'File_1C'

SELECT	@Flag		= Flag
FROM	GlobalParam
WHERE	OwnerId		= @RepoFlag
AND	Code		= @Code
AND	Kind		= @RepoFlag

IF	@@ROWCOUNT = 0	BEGIN

	SELECT	@Flag		= ISNULL( max( Flag ), 0 ) + 1
	FROM	GlobalParam
	WHERE	OwnerId		= @RepoFlag

	EXEC	dbo.sc_SetGlobalParam @RepoFlag, @Code, @RepoFlag, @Flag, 0, '��������� ��� ����� 1C', '', '', 1
END


--EXEC	dbo.sc_SetGlobalParam @RepoFlag, 'DrillDownMode'	,@Flag	,1	,0	,'���������� ������������ ���������'			,''	,'1 - ������������, 0 - �� ������������'	,	0x01
EXEC	dbo.sc_SetGlobalParam @RepoFlag, 'UU'		,@Flag	,0	,0	,'�������� �������� D060'			,'65;66;74;'	,'��� ��������� �������'		,	0
EXEC	dbo.sc_SetGlobalParam @RepoFlag, '100'		,@Flag	,0	,0	,'�������� ���������� A1C001'			,'3;2;1;'	,'����������� ������� ��'		,	0
EXEC	dbo.sc_SetGlobalParam @RepoFlag, '200'		,@Flag	,0	,0	,'�������� ���������� A1C002'			,'14282829;14282829;;','����-�������'			,	0
EXEC	dbo.sc_SetGlobalParam @RepoFlag, '300'		,@Flag	,0	,0	,'�������� ���������� A1C003'			,';2;1;'	,'������� ��'				,	0
EXEC	dbo.sc_SetGlobalParam @RepoFlag, '300_K025'	,@Flag	,0	,0	,'�������� �������� A1C003_K025'		,';14282829;;'	,'������ ��'				,	0
EXEC	dbo.sc_SetGlobalParam @RepoFlag, '310'		,@Flag	,0	,0	,'�������� ���������� A1C004'			,';;;;'		,'������� �� ����. ��������'		,	0
EXEC	dbo.sc_SetGlobalParam @RepoFlag, '320'		,@Flag	,0	,0	,'�������� ���������� A1C005'			,'2;2;2;2;'		,'������� ��'				,	0
EXEC	dbo.sc_SetGlobalParam @RepoFlag, 'A1C005_UU'	,@Flag	,0	,0	,'�������� �������� A1C005_UU'			,'65;65;66;66;66;66;74;74;74;'				,'��� ��������� �������'			,	0
EXEC	dbo.sc_SetGlobalParam @RepoFlag, 'A1C005_Z272'	,@Flag	,0	,0	,'�������� ���������� A1C005_Z272'		,'1;3;1;3;4;F;1;3;F;'					,'��� ���� ������������'			,	0


--EXEC	dbo.sc_SetGlobalParam @RepoFlag, '321'		,@Flag	,0	,0	,'�������� ���������� DDD = 321'		,'2;'		,'������� ��'					,	0
--EXEC	dbo.sc_SetGlobalParam @RepoFlag, '322'		,@Flag	,0	,0	,'�������� ���������� DDD = 322'		,'2;'		,'������� ��'					,	0
--EXEC	dbo.sc_SetGlobalParam @RepoFlag, '323'		,@Flag	,0	,0	,'�������� ���������� DDD = 323'		,'2;'		,'������� ��'					,	0
--EXEC	dbo.sc_SetGlobalParam @RepoFlag, '324'		,@Flag	,0	,0	,'�������� ���������� DDD = 324'		,'2;'		,'������� ��'					,	0
--EXEC	dbo.sc_SetGlobalParam @RepoFlag, 'DDD_310'		,@Flag	,0	,0	,'�������� ���������� DDD = 310'		,'4'	,'������� �� ����. ��������'			,	1
--EXEC	dbo.sc_SetGlobalParam @RepoFlag, 'CC25_310'		,@Flag	,0	,0	,'�������� ���������� ��=25 DDD = 310'		,'30'	,'������. ��'					,	1
--EXEC	dbo.sc_SetGlobalParam @RepoFlag, 'DDD_320'		,@Flag	,0	,0	,'�������� ���������� DDD = 320'		,'1'	,'������� ��'					,	1
--EXEC	dbo.sc_SetGlobalParam @RepoFlag, 'DDD_321'		,@Flag	,0	,0	,'�������� ���������� DDD = 321'		,'1'	,'������� ��'					,	1
--EXEC	dbo.sc_SetGlobalParam @RepoFlag, 'DDD_322'		,@Flag	,0	,0	,'�������� ���������� DDD = 322'		,'1'	,'������� ��'					,	1
--EXEC	dbo.sc_SetGlobalParam @RepoFlag, 'DDD_323'		,@Flag	,0	,0	,'�������� ���������� DDD = 323'		,'1'	,'������� ��'					,	1
--EXEC	dbo.sc_SetGlobalParam @RepoFlag, 'DDD_324'		,@Flag	,0	,0	,'�������� ���������� DDD = 324'		,'1'	,'������� ��'					,	1
--EXEC	dbo.sc_SetGlobalParam @RepoFlag, 'DDD_200'		,@Flag	,0	,0	,'�������� ���������� DDD = 200'		,''	,'����-�������'					,	1
--EXEC	dbo.sc_SetGlobalParam @RepoFlag, 'IsEmpDateInto'	,@Flag	,1	,0	,'���������� ���� ��������� - ������'		,''	,'1 - ������, 0 - ������������ �� ���������'	,	0x01

GO