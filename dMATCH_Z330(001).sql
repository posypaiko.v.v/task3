IF	object_id( 'dbo.MATCH_Z330' )	IS NOT NULL
	DROP	TABLE	dbo.MATCH_Z330
GO

CREATE	TABLE	dbo.MATCH_Z330
(	Id		TRowId		IDENTITY	CONSTRAINT I_MATCH_Z330_Id		PRIMARY KEY NONCLUSTERED
,	RefferId	TRowId		NULL		CONSTRAINT R_MATCH_Z330_RefferId	REFERENCES RefferParam( Id )
												ON DELETE CASCADE				
,	Kind		TRowId		NOT NULL
,	Stamp		timestamp	NOT NULL

,	Z330		VarChar(1)	DEFAULT('')
,	Z270		TCode		DEFAULT('')
,	PosData		TCode		DEFAULT('')
,	OperCode	TCode		DEFAULT('')
,	ReasonCode	TCode		DEFAULT('')
)
GO

CREATE CLUSTERED INDEX I_MATCH_Z330_Kind ON dbo.MATCH_Z330( Kind, Id )
GO
CREATE INDEX I_MATCH_Z330_RefferId ON dbo.MATCH_Z330( RefferId, Id )
GO
--	MATCH_Z330.DBF
EXEC	dbo.ui_SetRefferOption	@RefferCode = 'MATCH_Z330.DBF', @TypeFlag = 5, @Flag = 0
EXEC	dbo.ui_SetRefferList	@RefferCode = 'MATCH_Z330.DBF', @ParamCode = 'Z330', @Flag = 1
EXEC	dbo.ui_SetRefferList	@RefferCode = 'MATCH_Z330.DBF', @ParamCode = 'Z270', @Flag = 2
EXEC	dbo.ui_SetRefferList	@RefferCode = 'MATCH_Z330.DBF', @ParamCode = 'PosData', @Flag = 2
EXEC	dbo.ui_SetRefferList	@RefferCode = 'MATCH_Z330.DBF', @ParamCode = 'OperCode', @Flag = 2
EXEC	dbo.ui_SetRefferList	@RefferCode = 'MATCH_Z330.DBF', @ParamCode = 'ReasonCode', @Flag = 2

GO

GRANT SELECT, INSERT, UPDATE, DELETE ON dbo.MATCH_Z330	TO sc_Servers, sc_SysOps, sc_Keepers, sc_Operators
GO

