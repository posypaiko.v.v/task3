IF	OBJECT_ID('dbo.RepoStore_ApplicationsN1')	IS NOT NULL
	DROP	TABLE	dbo.RepoStore_ApplicationsN1
GO
---------------------------------------------------------------------------------------------------
--	SELECT	TOP 1000 * FROM RepoStore_ApplicationsN1
---------------------------------------------------------------------------------------------------
--	������ ��������� �� ������ ���������� �������
CREATE	TABLE	dbo.RepoStore_ApplicationsN1
(	Id		TRowId		IDENTITY	PRIMARY KEY NONCLUSTERED
,	ReportDate	TDate		NOT NULL	-- ���� �������� ������ �� �������� � ��� (��������� ����������� ���� ������)												,
,	DateAppl	TDate		NOT NULL	-- ���� ���������� ������
,	IdAppl		TRowId		NOT NULL	-- �� ������
,	DateConcl	TDate		NULL		-- ���� ������ �� ������
,	ClientId	TRowId		NOT NULL	--�� ������� � ���
,	ProductId	TRowId		NOT NULL	--�� ���������� �������� � ���
,	Confirmed	TMoney		NOT NULL	--����� � ��������
,	CurrencyTag	TTag		NOT NULL	--������
,	BranchId	TRowId		NOT NULL	--�� ������� � ���
,	Comment		TInfo		NULL		--����������� �� ������

	CONSTRAINT IRepoStore_ApplicationsN1Identity
	UNIQUE CLUSTERED
	(	ReportDate
	,	Id
	)	,
)
GO