IF	OBJECT_ID('dbo.RepoXMLStoreLinks_N2_TAB')	IS NOT NULL
	DROP	TABLE dbo.RepoXMLStoreLinks_N2_TAB
GO

CREATE	TABLE	dbo.RepoXMLStoreLinks_N2_TAB
(	TLinkId		TRowId		NOT NULL	,
	XRowNo		TRowId		NOT NULL	,
	TRowNo		TRowId		NOT NULL	,

	CONSTRAINT IRepoXMLStoreLinks_N2_TAB_Identity	PRIMARY	KEY
	(	TLinkId	,
		TRowNo	,
		XRowNo
	),
)
GO
